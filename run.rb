#!/usr/bin/ruby
require 'discordrb'
require 'sqlite3'
require_relative 'credentials'

db = SQLite3::Database.new "groups.db"

db.execute('CREATE TABLE IF NOT EXISTS groupes (serverid INTEGER,groupid VARCHAR(255));')

def print_exception(exception, explicit)
  puts "[#{explicit ? 'EXPLICIT' : 'INEXPLICIT'}] #{exception.class}: #{exception.message}"
  puts exception.backtrace.join("\n")
end

def rolesid(event)
  event.server.roles.each do |role|
    event.respond(role.name + " a pour id `" + role.id.to_s + "`")
  end
end

bordel_en_cours = false
bot = Discordrb::Commands::CommandBot.new token: Credentials::BOT_TOKEN, prefix: '!'

# bot.message do |event|
#   event.respond 'Zizi !'
# end


# bot.command :user do |event|
#   # Commands send whatever is returned from the block to the channel. This allows for compact commands like this,
#   # but you have to be aware of this so you don't accidentally return something you didn't intend to.
#   # To prevent the return value to be sent to the channel, you can just return `nil`.
#   event.user.name
# end
#
# bot.command :bold do |_event, *args|
#   # Again, the return value of the block is sent to the channel
#   "**#{args.join(' ')}**"
# end

bot.ready do |event|
  puts "Bot lancé. Son \"Bordel à cul de pompe à merde\" depuis le site Wiktionary. "
end

bot.command(:rejoindre, description: 'Permet de rejoindre le groupe que vous désirez.') do |event, *args|
  break if event.server.nil? # Si on n'est pas dans un serveur, on ne traite pas la commande.
  nomgroupe = args.join(' ')
  if nomgroupe != ''
    groupids = []
    db.execute('SELECT groupid FROM groupes WHERE serverid = ?', [event.server.id]) do |row|
      groupids.push row[0]
    end

    if groupids.length.zero?
      event.respond 'Vous ne pouvez pas rejoindre de groupe sur ce serveur.'
      break
    end

    roleselected = nil

    event.server.roles.each do |role|
      if role.name.upcase == nomgroupe.upcase
        roleselected = role
        break
      end
    end

    if roleselected.nil?
      event.respond 'Le groupe demandé n\'existe pas sur ce Discord.'
      break
    end

    roleid = roleselected.id.to_s
    if groupids.include?(roleid)
      if event.author.role?(roleselected)
        event.respond 'Vous ne pouvez pas rejoindre un groupe que vous avez déjà rejoint.'
        break
      end
      begin
        event.author.add_role(roleselected)
        event.respond 'Vous avez bien été ajouté au groupe **' + roleselected.name + '** !'
      rescue StandardError => e
        print_exception(e, true)
        event.respond "Un problème est survenu au moment de rejoindre ce groupe. Vérifiez que le bot a bien les permissions, et contactez éventuellement le développeur du bot (<@#{Credentials::BOT_OWNER}>)."
      end
    else
      event.respond 'Vous n\'avez pas le droit de rejoindre ce groupe.'
      break
    end
  else
    event.respond('Vous devez spécifier un nom de groupe.')
    break
  end
end

bot.command(:ajouter, help_available: false) do |event, *args|
  break if event.server.nil?
  if event.user.id != 113758654271848457 and !event.author.permission?(:manage_roles)
    event.respond('Vous ne pouvez pas utiliser cette commande.')
    break
  end
  nomgroupe = args.join(' ')
  if nomgroupe == ''
    event.respond("Vous devez spécifier un nom de groupe.")
    break
  end

  rolesnames = {}
  event.server.roles.each do |role|
    rolesnames[role.name.upcase] = role.id
  end

  rolechoisi = rolesnames[nomgroupe.upcase]
  if rolechoisi.nil?
    event.respond('Le groupe que vous désirez ajouter n\'existe pas.')
    break
  end
  nombrelignes = 0
  db.execute('SELECT COUNT(*) FROM groupes WHERE groupid = ? AND serverid = ?', [rolechoisi,event.server.id]) do |row|
    nombrelignes = row[0]
  end

  if nombrelignes.nonzero?
    event.respond('Le groupe peut déjà être rejoint.')
    break
  end

  db.execute('INSERT INTO groupes (groupid, serverid) VALUES (?, ?);', [rolechoisi, event.server.id])
  event.respond('Le groupe peut maintenant être rejoint.')
end

bot.command(:liste, description: 'Liste les groupes pouvant être rejoints par une commande sur ce serveur.') do |event|
  break if event.server.nil?
  nbgroupes = 0
  db.execute('SELECT COUNT(*) FROM groupes WHERE serverid = ?', [event.server.id]) do |row|
    nbgroupes += row[0]
  end
  if nbgroupes.nonzero?
    # message = '<ul>'
    # message = '```'
    message = "Les groupes suivants peuvent être rejoints :\n"
    db.execute('SELECT groupid FROM groupes WHERE serverid = ?', [event.server.id]) do |row|
      # message += "<li><b>#{event.server.role(row[0]).name}</b></li>"
      message += "- ``#{event.server.role(row[0]).name}``\n"
    end
    # message += '</ul>'
    # message += '```'
    # embed =  Discordrb::Webhooks::Embed.new(title: 'Liste des groupes que vous pouvez rejoindre', color: 0x4169E1)
    # embed = Discordrb::Webhooks::Embed.new
    # bot.send_message(event.channel.id, message, embed)
    bot.send_message(event.channel.id, message)
  else
    event.respond('Aucun groupe ne peut être rejoint sur ce serveur.')
  end
end

bot.command(:quitter, description: 'Quittez un groupe auquel vous appartenez.') do |event, *args|
  break if event.server.nil?
  groupname = args.join(' ')
  if groupname == ''
    event.respond('Vous devez spécifier quel groupe vous souhaitez quitter.')
    break
  end

  groupesactuels = {}
  event.author.roles.each do |role|
    groupesactuels[role.name.upcase] = role.id
  end

  roleid = groupesactuels[groupname.upcase]
  if roleid.nil?
    event.respond('Vous n\'avez pas le rôle demandé.')
    break
  end

  roleautorise = 0
  db.execute('SELECT COUNT(*) FROM groupes WHERE groupid = ? AND serverid = ?', [roleid,event.server.id]) do |row|
    roleautorise = row[0]
  end

  if roleautorise.zero?
    event.respond('Vous ne pouvez pas utiliser le bot pour changer ce grade. Demandez à un administrateur.')
    break
  end

  rolesupprime = event.server.role(roleid)
  begin
    event.author.remove_role(rolesupprime)
    event.respond('Le groupe **' + rolesupprime.name + '** vous a été retiré.')
  rescue StandardError => e
    event.respond("Un problème est survenu lors de l'action. Vérifiez que le bot puisse vous attribuer les droits, et contactez le développeur du bot (<@#{Credentials::BOT_OWNER}>).")
  end
end

bot.command(:bordel, description: 'Bordel à cul de pompe à merde.') do | event |
  break if bordel_en_cours
  break if event.server.nil?

  if event.author.voice_channel.nil?
    event.respond('Vous devez être dans un salon vocal pour effectuer cette commande.')
    break
  end
  begin
    bordel_en_cours = true
    vb = bot.voice_connect(event.author.voice_channel)
    vb.play_file "bordel.ogg"
  rescue Error => e
    event.respond("Le bot ne peut pas rejoindre ce salon vocal.")
  end
  vb.destroy
  bordel_en_cours = false
  break
end

bot.command(:retirer, help_available: false) do |event, *args|
  break if event.server.nil?
  if event.user.id != 113758654271848457 and !event.author.permission?(:manage_roles)
    event.respond('Vous ne pouvez pas utiliser cette commande.')
    break
  end

  groupname = args.join(' ')
  if groupname == ''
    event.respond('Vous devez spécifier quel groupe vous souhaitez retirer.')
    break
  end

  groupesactuels = {}
  event.server.roles.each do |role|
    groupesactuels[role.name.upcase] = role.id
  end

  roleid = groupesactuels[groupname.upcase]
  if roleid.nil?
    event.respond('Le groupe n\'existe pas.')
    break
  end

  roleautorise = 0
  db.execute('SELECT COUNT(*) FROM groupes WHERE groupid = ? AND serverid = ?', [roleid,event.server.id]) do |row|
    roleautorise = row[0]
  end

  if roleautorise.zero?
    event.respond('Le rôle ne peut pas être rejoint. Il est donc impossible de le supprimer')
    break
  end

  begin
    db.execute('DELETE FROM groupes WHERE groupid = ? AND serverid = ?', [roleid, event.server.id])
    event.respond('Le rôle ne peut désormais plus être modifié avec le bot.')
  rescue StandardError => e
    print_exception(e, true)
    event.respond("Une erreur est survenue durant la suppression du groupe. Contactez le développeur du bot (<@#{Credentials::BOT_OWNER}>).")
  end
end

bot.command(:eval, help_available: false) do |event, *code|
  break unless event.user.id == Credentials::BOT_OWNER # Replace number with your ID
  begin
    eval code.join(' ')
  rescue StandardError => e
    print_exception(e,true)
    'An error occurred 😞'
  end
end

bot.run
