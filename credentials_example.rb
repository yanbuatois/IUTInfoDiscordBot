module Credentials
  BOT_TOKEN = 'insert_token_here' # This string stores the Discord bot's token.
  BOT_OWNER = 0 # Bot owner id, for mentions and !eval command.
  COMMAND_PREFIX = '!' # Prefix for bot commands. ! by default.
end